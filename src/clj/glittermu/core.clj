(ns glittermu.core
  (:require [clojure.core.async :refer [<! >! go go-loop] :as async]
            [clojure.stacktrace :refer [print-stack-trace]]
            [clojure.string :as str]

            [chord.http-kit :refer [with-channel]]

            [org.httpkit.server :refer [run-server]]

            [ring.middleware.params :refer [params-request]]
            [ring.util.response :as response]

            [compojure.core :refer [defroutes GET rfn]]
            [compojure.route :refer [resources]]

            [taoensso.timbre :as timbre :refer [info warn error]]
            [taoensso.timbre.appenders.rotor :as rotor]

            [glittermu.db :as db]
            [glittermu.messages :as msg]
            [glittermu.utils :as $]))

(timbre/set-config!
 [:appenders :rotor]
 {:min-level :info
  :enabled? true
  :async false
  :max-message-per-msecs nil
  :fn rotor/appender-fn})

(timbre/set-config!
 [:shared-appender-config :rotor]
 {:path "glittermu.log" :max-size (* 1024 1024) :backlog 10})



(defn login-view
  [req]
  (response/file-response "resources/public/index.html"))

(def commands
  {"say" (fn [u arg]
           (db/otell-all u {:type :message
                            :user (db/id u)
                            :text (str " says, \"" arg "\"")}))
   "pose" (fn [u arg]
            (db/otell-all u {:type :message
                             :user (db/id u)
                             :text (str " " arg)}))
   "spoof" (fn [u arg]
             (db/tell u {:type :message
                         :text "I haven't implemented spoof yet!"}))
   "desc" (fn [u arg]
            (try
              (db/set-prop-string! (db/location u) :description arg
                                   {:publish true})
              (db/tell u {:type :message
                          :text "Description changed."})
              (catch Exception exc
                (info exc)
                (db/tell u {:type :error
                            :text (str "Could not set property:" exc)}))))

   "who" (fn [u arg]
           (let [users (db/who)]
             (db/tell u {:type :message
                         :text (str "Online: " ($/commas (map db/name (db/who))))
                         })))
   "hug" (fn [u arg]
           (db/otell-all u {:type :message
                            :user (db/name u)
                            :text (str " hugs " ($/commas (map db/name (db/contents (db/location u)))) "!")}))})

(defn process-message
  [user s]
  (let [[handler arg] (condp re-find s
                        #"^\"" [(get commands "say") (str/triml (subs s 1))]
                        #"^:" [(get commands "pose") (str/triml (subs s 1))]

                        ;; default:
                        (let [[cmd arg] (str/split s #" " 2)]
                          [(get commands cmd) arg]))]
    (if handler
      (handler user arg)
      (db/tell user {:type :message
                     :text "Huh?"}))))

(defn use
  [user what-id]
  (if-let [what (db/db-get what-id)]
    (if (db/can-use? user what)
      (case (:type what)
        :exit (do (when-let [to (:goto @what)]
                    (db/set-location! user to))
                  (when-let [succ (db/get-prop @what :sucess)]
                    (db/tell user succ)))

        (db/tell user {:text "You can't use that."}))

      (db/tell user {:text "You can't use that."}))
    (db/tell user {:text "I don't know what that is."})))

(defn connect
  [{:keys [params] :as req}]
  (info req)
  (let [name (get params "user" "guest")
        [new? user] (db/get-or-make-user name)]
    (with-channel req ws-ch nil
      (info "CONNECTED:" name "-" user)
      (db/add-connection! user ws-ch)
      (go
        (>! ws-ch {:type :message
                   :text (str (if new? "Welcome, " "Welcome back, ") name "! Here--have some glitter!")})
        (db/notify (db/location user) (msg/connected user))
        (db/tell-location user)
        (loop []
          (when-let [{m :message} (<! ws-ch)]
            (info "RECEIVED from" name ":" m)
            (case (:type m)
              :message (process-message user (:text m))
              :use (use user (:what m))
              (info "UNRECOGNIZED TYPE:" (:type m)))
            (recur)))

        ;; All done
        (db/remove-connection! user ws-ch)
        (db/notify (db/location user) (msg/disconnected user))
        (info "DISCONNECTED:" name)))))


(defroutes glittermu
  (GET "/" [] login-view)
  (GET "/connect" req (connect (params-request req)))
  (resources "/"))

(defn wrap-exceptions
  [handler]
  (fn [req]
    (try
      (handler req)
      (catch Exception exc
        (let [err-s (with-out-str
                      (print-stack-trace exc))]
          (info err-s)
          (-> (response/response
               err-s)
              (response/status 500)))))))


(def app (-> glittermu
             wrap-exceptions))

(defn -main [& [cmd & args]]
  (run-server #'app {:port 3337})
  (println "Started server on port 3337"))
