(ns glittermu.repl
  (:require [org.httpkit.server :refer [run-server]]

            [glittermu.core]
            [glittermu.store :as store]))

(def stop-server-fn (atom nil))

(defn stop-server
  []
  (swap! stop-server-fn (fn [f] (when f (f)))))

(def port 3337)

(defn start-server
  ([port]
   (swap! stop-server-fn
          (fn [f]
            (when f (f))
            (run-server #'glittermu.core/app {:port port}))))
  ([]
   (start-server port)))

(defn start-with-default
  []
  (store/load-default-db!)
  (start-server port))
