(ns glittermu.utils)

(defn commas
  [l]
  (let [bl (butlast l)]
    (str
     (apply str (interpose ", " bl))
     (when bl ", and ")
     (last l))))
