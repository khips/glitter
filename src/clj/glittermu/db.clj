(ns glittermu.db
  (:require [clojure.core.async :as async]

            [taoensso.timbre :refer [info]])
  (:refer-clojure :exclude [name]))

(remove-method print-method clojure.lang.IDeref)

(def dbtop
  "The last used dbid."
  (ref -1))

(def db
  (ref {}))

(def users
  "A mapping of username -> dbref"
  (ref {}))

(defn db-get
  [id]
  (get @db id))

(declare publish-changes location)
(defn make-new
  [m]
  (dosync
   (let [props (atom (:props m))
         objid (alter dbtop inc)
         objref (ref (assoc m
                       :props props
                       :id objid))]
     (alter db assoc objid objref)
     (publish-changes objref :neighbor-publish location)
     (publish-changes objref :contents-publish identity)

     [objid objref])))

(defn get-user
  [name]
  (get @users name))

(declare tell tell-move prop-summary tell-location)
(defn set-location!
  [what where]
  (let [where (if (integer? where) (db-get where) where)
        last-where (:location @what)]
    (dosync
     (when last-where
       (alter last-where #(assoc % :contents (remove (partial = what) (:contents %)))))
     (alter what assoc :location where)
     (alter where #(assoc % :contents (cons what (:contents %))))

     (tell-location what where)
     (tell-move what last-where where))))

(defn get-or-make-user
  "Returns [new? uref] where new? is true if the user was newly
  created. id and ref are the dbid and ref of the user."
  [name]
  (if-let [uref (get-user name)]
    [false uref]
    [true (dosync
           (let [[uid uref] (make-new {:type :user
                                       :name name})]
             (alter users assoc name uref)
             (set-location! uref (db-get 0))
             uref))]))

(defn contents
  "Contents is a list of refs."
  [what]
  (when-let [what @what]
    (:contents what)))

(defn name
  [what]
  (:name @what))

(defn id
  [what]
  (:id @what))

(defn location
  [what]
  (:location @what))

(declare prop-summary)

(defmulti type-summary (comp :type deref))

(defmethod type-summary :user
  [u]
  {:online (pos? (count (:connections @u)))})
(defmethod type-summary :default [_] nil)


(defn summary
  "Returns a map summarizing the given dbref."
  [what]
  (merge {:name (name what)
          :type (:type @what)
          :id (id what)
          :props (prop-summary what)}
         (type-summary what)))

(defn db-summary
  "Returns a mapping of id -> map, optionally including information
  about the contents of the given dbref, if contents? is true."
  [what & [contents?]]
  (cond-> {(id what) (merge {:name (name what)
                             :type (:type @what)
                             :id (id what)
                             :props (prop-summary what)
                             :contents (map id (contents what))}
                            (type-summary what))}

          ;; Add information about the
          contents?
          (merge
           (persistent!
            (reduce
             (fn [m w] (assoc! m (id w) (summary w)))
             (transient {})
             (contents what))))))

;; Connection management and messaging:
(defn add-connection!
  [what c]
  (dosync
   (alter what #(assoc % :connections (cons c (:connections %))))))

(defn remove-connection!
  [what c]
  (dosync
   (alter what #(assoc % :connections (remove (partial = c) (:connections %))))))

(defn connections
  [what]
  (:connections @what))

(defn tell
  [what m]
  (when-let [what @what]
    (let [conns (:connections what)]
      (doseq [conn conns]
        (async/put! conn m)))))

(defn notify
  "Notify the contents of an object."
  [where m]
  (doseq [what (contents where)]
    (tell what m)))

(defn notify-except
  "exclude should be a set of refs to exclude."
  [where m exclude]
  (doseq [what (contents where)]
    (when-not (contains? exclude what)
      (tell what m))))

(defn otell
  "Send a message to everyone in what's location."
  [what m]
  (notify-except (location what) m #{what}))

(defn otell-all
  [what m]
  (notify (location what) m))

(defn who
  "Returns a sequence of the users currently connected."
  []
  (filter (fn [u]
            (pos? (count (:connections @u))))
          (map val @users)))


;; Properties:
;; Properties can be strings or maps.
;; Map:
;;  { :string  ; the string contents of the property
;;    :code    ; string
;;    :publish ; boolean
;;    :public  ; boolean
;;  }
(defn props
  "Returns the atom containing the dbref's properties."
  [what]
  (:props @what))

(defn get-prop
  [what prop]
  (some-> what props deref (get prop)))

(defn desc [what]
  (or (get-prop what :description)
      "You see nothing special."))

(defn publish?
  "Should changes to this property be pushed to unprivileged watchers?"
  [prop]
  (or (string? prop)
      (:publish prop)))

(defn public?
  "Can the property be viewed by unprivileged users?"
  [prop]
  (or (string? prop)
      (:public prop)
      (:publish prop)))

(defn prop-summary
  "Returns a map containing the properties of the dbref marked for
  publication."
  [what]
  (into {} (filter (comp publish? val) (deref (props what)))))

(defn set-prop-string!
  ([what pname pval opts]
   (let [patom (props what)
         prop (merge {:string (str pval)} opts)]
     (swap! patom assoc pname prop)))
  ([what pname pval]
   (set-prop-string! what pname pval nil)))

(defn publish-changes
  "When properties on what marked 'publish' are changed, push those
  changes to the contents of (where-fn what)."
  [what k where-fn]
  (let [patom (props what)
        dbid (id what)]
    (add-watch patom k
               (fn [_ _ old new]
                 (doseq [k (into #{} (concat (keys old) (keys new)))]
                   (let [newval (get new k)]
                     (when-not (= (get old k) newval)
                       (when-let [where (where-fn what)]
                         (notify where
                                 {:type :prop-change
                                  :prop k
                                  :dbid dbid
                                  :newval newval})))))))))

(defn tell-location
  "Send a message to "
  ([what where]
   (tell what {:type :location
               :location (id where)
               :db (db-summary where true)}))
  ([what]
   (tell-location what (location what))))

(defn tell-move
  "Send a message to the room that what has just left and to the room
  that what is entering."
  [what last-where now-where]
  (let [m {:type :move
           :what (id what)
           :db (db-summary what)
           :where (id now-where)
           :last-where (when last-where (id last-where))}]
    (when last-where
      (notify last-where m))
    (notify-except now-where m #{what})))

;; Using
(defn can-use?
  [who what]
  (or (= (location who) (location what))
      (= (location what) who)))

(defn can-use-id?
  [who what-id]
  (can-use? who (db-get what-id)))
