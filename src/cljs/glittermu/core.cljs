(ns glittermu.core
  (:require [cljs.core.async :refer [>! <!] :as async]
            [clojure.set :as set]
            [clojure.string :as str]

            [goog.dom :as dom]
            [goog.dom.classes :as classes]
            [goog.events :as events]
            [goog.events.KeyCodes :as keycode]
            [goog.string :as string]

            [om.core :as om :include-macros true]
            [om.dom :as om-dom :include-macros true]

            [chord.client :refer [ws-ch]]

            [figwheel.client :as fw]

            [glittermu.utils :as $])
  (:require-macros [cljs.core.async.macros :refer [go]]))

(enable-console-print!)

(defonce connection
  (atom nil))

(defonce app-state
  (atom {:db-cache {}
         :location nil}))

(defn db-get
  [id]
  (get-in @app-state [:db-cache id]))

(defn send-message!
  [msg]
  (when-let [conn @connection]
    (async/put! conn msg)))

(defn connect!
  [handler name]
  (go
    (let [url (str "ws://"
                   (.-host js/location)
                   "/connect?user="
                   (string/htmlEscape name))
          {:keys [ws-channel error]} (<! (ws-ch url))]
      (if-not error
        (do
          (reset! connection ws-channel)
          (loop []
            (let [{m :message, err :error} (<! ws-channel)]
              (if err
                nil
                (do
                  (handler m)
                  (recur))))))))))

(defn prn-through
  [x]
  (prn x)
  x)

(defn ids->objects
  ([db ids]
   (prn-through (persistent!
                 (reduce (fn [v id]
                           (conj! v (get db id)))
                         (transient [])
                         ids)))))

(defn eval-prop [prop & [default]]
  (cond
   (string? prop) prop

   (:code prop) (try
                  (or ((js/eval (:code prop))) default)
                  (catch js/Error err
                    (str "Error while evaluating code:\n"
                         err
                         "\n\n"
                         (:string prop))))

   (:string prop) (:string prop)

   (not prop) default))

;; OM Components:
(defn summary-view
  ""
  [thing owner]
  (reify
    om/IRender
    (render [this]
      (om-dom/span #js {:className
                        (str/join " " ["summary"
                                       (name (:type thing))
                                       (when (contains? thing :online)
                                         (if (:online thing)
                                           "online" "offline"))])}
                   (string/htmlEscape (:name thing))))))

(defn exit-view
  "Render a single exit."
  [exit owner]
  (reify
    om/IRender
    (render [this]
      (om-dom/div #js {:className "exit"}
                  (om-dom/a #js {:href "#"
                                 :gm_dbid (:id exit)}
                            (string/htmlEscape (:name exit)))))))

(defn location-view
  [app owner]
  (reify
    om/IRender
    (render [this]
      (let [db (:db-cache app)
            loc (get db (:location app))
            contents (ids->objects db (:contents loc))]
        (om-dom/div #js {:id "location"}
                    (om-dom/h3 #js {:id "loc_name"} (:name loc))
                    (om-dom/div #js {:id "loc_desc"}
                                (eval-prop
                                 (:description (:props loc))
                                 "You see nothing special."))
                    (when-let [exits (seq (filter #(= (:type %) :exit) contents))]
                      (om-dom/div #js {:id "loc_exits"}
                                  (om/build-all exit-view exits)))
                    (apply om-dom/div #js {:id "loc_users"
                                           :className "summary_list"}
                           (om-dom/span #js {:className "label"} "Users:")
                           (om/build-all summary-view contents)))))))

(defn append-message
  "actor should be the name of the user performing the action.
  Returns the new message div element."
  [actor text]
  (let [new-msg (dom/createDom "div" #js {"class" "message"}
                               (when actor
                                 (dom/createDom "span" #js {"class" "actor"} actor))
                               text)
        out ($/by-id "output")]
    (dom/append out new-msg)
    (when (< ($/scroll-dist-from-bottom out)
             (+ 5 (.-offsetHeight new-msg)))
      ($/scroll-to-bottom! out))
    new-msg))


(defn set-location!
  [loc-id]
  (swap! app-state assoc :location loc-id))

(defn set-prop!
  [{:keys [dbid prop newval]}]
  (swap! app-state assoc-in [:db-cache dbid :props prop] newval))

(defn handle-message
  [m]
  (prn "Received:" m)
  (let [{t :text, db :db} m
        user (db-get (:user m))
        uname (string/htmlEscape (:name user ""))]
    (when db
      ;; Update the DB. New values will supersede old.
      (swap! app-state (fn [st]
                         (let [dbc (:db-cache st)]
                           (assoc st
                             :db-cache (merge-with merge dbc db))))))

    (case (:type m)
      :error (doto (append-message nil t)
               (classes/add "error"))
      :connect (append-message uname " has connected.")
      :disconnect (append-message uname " has disconnected.")
      :location (set-location! (:location m))
      :prop-change (set-prop! m)
      nil)

    ;; Any message can have a visible component that is printed
    ;; directly.
    (when-let [t (:text m)]
      (cond
       (string? t)
       (append-message (when-not (:hide-name m) uname) t)

       (or (list? t) (vector? t))
       (doseq [t t]
         (append-message uname t))))))

(defonce listener-keys (atom nil))
(defn unlisten!
  []
  (doseq [k @listener-keys]
    (events/unlistenByKey k))
  (reset! listener-keys nil))

(defn setup-input!
  []
  (when-let [input ($/by-id "input")]
    (swap! listener-keys conj
           (events/listen input "keypress"
                          (fn [e]
                            (let [input (.-target e)]
                              (when (and (= (.-keyCode e) keycode/ENTER)
                                         (not (.-shiftKey e)))
                                (when-let [conn @connection]
                                  (async/put! conn {:type :message
                                                    :text (.-value input)})
                                  (set! (.-value input) "")
                                  (.preventDefault e)))))))))

(defn choose-nick
  []
  (loop []
    (let [n (js/prompt "Please choose a nickname (at least 5 characters long):")]
      (if (< (.-length n) 5)
        (recur)
        n))))

(defn init []
  (unlisten!)
  (when-not @connection
    (connect! (fn [m] (handle-message m)) (choose-nick)))
  (setup-input!))

(init)

(om/root location-view app-state
         {:target ($/by-id "loc_mount")})

(fw/start {:on-jsload (fn []
                        (println "Reloaded JavaScript.")
                        (init))
           :websocket-url "ws://kssh.ca:3338/figwheel-ws"})
