(ns glittermu.utils
  (:require [goog.dom :as dom]
            [goog.string :as string]))

(defn by-id
  [id]
  (.getElementById js/document id))


(defn scroll-to-bottom!
  [elt]
  (set! (.-scrollTop elt) (.-scrollHeight elt)))

(defn scroll-dist-from-bottom
  [elt]
  (- (.-scrollHeight elt)
     (+ (.-scrollTop elt) (.-offsetHeight elt))))
