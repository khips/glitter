(defproject glittermu "1.0.0-SNAPSHOT"
  :description "FIXME: write description"
  :dependencies [[org.clojure/clojure "1.6.0"]
                 [org.clojure/clojurescript "0.0-2496"]
                 [org.clojure/core.async "0.1.346.0-17112a-alpha"]

                 [figwheel "0.2.0-SNAPSHOT"]

                 [com.taoensso/timbre "3.2.1"]
                 [jarohen/chord "0.4.1" :exclusions [org.clojure/clojure]]
                 [ring/ring-core "1.3.0"]
                 [compojure "1.3.1"]

                 [om "0.8.0-beta3"]]

  :main glittermu.core

 ;:repl-options {:init-ns glittermu.repl}
  :repl-options {:init-ns glittermu.repl}

  :source-paths ["src/clj" "src/cljs"]

  :plugins [[lein-cljsbuild "1.0.3"]
            [lein-figwheel "0.2.0-SNAPSHOT"]
            [cider/cider-nrepl "0.8.1"]]

  :cljsbuild {:builds [{:id "dev"
                        :source-paths ["src/cljs", "target/generated/cljs"]
                        :compiler {:output-to "resources/public/js/app.js"
                                   :output-dir "resources/public/js/out"
                                   :optimizations :none
                                   :preamble []
                                   :source-map true}}
                       {:id "release"
                        :source-paths ["src/cljs", "target/generated/cljs"]
                        :compiler {:output-to "resources/public/js/app.js"
                                   :optimizations :advanced
                                   :preamble []
                                   :pretty-print false}}]}

  :figwheel {:server-port 3338
             :http-server-root "public"

             :css-dirs ["resources/public/css"]})
